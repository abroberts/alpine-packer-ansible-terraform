# syntax=docker/dockerfile:1
FROM alpine:latest
RUN apk update \
    # Install supporting tools
    && apk add --no-cache jq unzip curl git py3-pip python3 \
    # Install Packer
    packer \
    # Install Ansible
    ansible \
    # Install pywinrm
    && pip3 install "pywinrm>=0.2.2" \
    # Install Terraform Current Version
    && curl "https://releases.hashicorp.com/terraform/$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')/terraform_$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')_linux_amd64.zip" -o terraform_current_linux_amd64.zip \
    #&& unzip terraform_current_linux_amd64.zip -d /usr/bin \
    && unzip terraform_current_linux_amd64.zip \
    && mv terraform /usr/bin \
    && rm terraform_current_linux_amd64.zip